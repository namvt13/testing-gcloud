const assert = require('assert');
const sinon = require('sinon');
const uuid = require('uuid');

// Get the index file in the parent dir
const { helloHttp } = require('..');

it('helloHttp: should print a name', () => {
  // Mocks ExpressJS 'req' and 'res' parameters
  const name = uuid.v4();
  // Create a fake request
  const req = {
    query: {},
    body: {
      name,
    }
  };

  const res = { send: sinon.stub() };
  // Call tested function
  helloHttp(req, res);
  // Verify behavior of tested function, check if send is called with params
  assert.ok(res.send.calledOnce)
  assert.deepStrictEqual(res.send.firstCall.args, [`Hello ${name}!`])
});
